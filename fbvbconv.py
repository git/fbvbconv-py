#!/usr/bin/python3
"""
FRITZ!Box voicebox converter
input: meta* rec/rec.*
output: wav/<timestamp>-<calling number>.wav
requires libspeex1
tested on Debian sid with Python 3.5.2

voicebox meta extraction and speex conversion
algorithm taken from roger-router-1.9.3

Copyright (C) 2017 Johannes Stezenbach

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
"""

import sys, os
import struct
import ctypes
import wave

speexlib = ctypes.cdll.LoadLibrary("libspeex.so.1")
SPEEX_SET_SAMPLING_RATE = 24
SPEEX_GET_FRAME_SIZE = 3

class SpeexMode(ctypes.c_void_p):
    pass
class Speex(ctypes.c_void_p):
    pass
class SpeexBits(ctypes.Structure):
    _fields_ = [
        ('chars', ctypes.c_char_p),
        ('nbBits', ctypes.c_int),
        ('charPtr', ctypes.c_int),
        ('bitPtr', ctypes.c_int),
        ('owner', ctypes.c_int),
        ('overflow', ctypes.c_int),
        ('but_size', ctypes.c_int),
        ('reserved1', ctypes.c_int),
        ('reserved2', ctypes.c_void_p)
    ]

speex_lib_get_mode = speexlib.speex_lib_get_mode
speex_lib_get_mode.restype = SpeexMode
speex_decoder_init = speexlib.speex_decoder_init
speex_decoder_init.restype = Speex
speex_decoder_ctl = speexlib.speex_decoder_ctl
speex_bits_init = speexlib.speex_bits_init
speex_bits_read_from = speexlib.speex_bits_read_from
speex_decode_int = speexlib.speex_decode_int
speex_bits_remaining = speexlib.speex_bits_remaining
speex_bits_destroy = speexlib.speex_bits_destroy


class VoiceboxMeta(ctypes.BigEndianStructure):
    _fields_ = [
        ("len", ctypes.c_int),
        ("index", ctypes.c_byte * 4), # little endian
        ("type", ctypes.c_int),
        ("sub_type", ctypes.c_uint),
        ("size", ctypes.c_uint),
        ("duration", ctypes.c_uint),
        ("status", ctypes.c_uint),
        ("pad0", ctypes.c_byte * 24),
        ("remote_number", ctypes.c_char * 54),
        ("pad1", ctypes.c_byte * 18),
        ("file", ctypes.c_char * 32),
        ("path", ctypes.c_char * 128),
        ("day", ctypes.c_byte),
        ("month", ctypes.c_byte),
        ("year", ctypes.c_byte),
        ("hour", ctypes.c_byte),
        ("minute", ctypes.c_byte),
        ("pad2", ctypes.c_byte * 31),
        ("local_number", ctypes.c_char * 24),
        ("pad3", ctypes.c_byte * 4),
    ]

metalen = ctypes.sizeof(VoiceboxMeta)


def speex_convert(inp, outp):
    rec = open(inp, 'rb').read()
    wav = wave.open(outp, 'wb')
    wav.setnchannels(1)
    wav.setsampwidth(2)
    wav.setframerate(8000)

    mode = speex_lib_get_mode(0)
    speex = speex_decoder_init(mode)
    speex_decoder_ctl(speex, SPEEX_SET_SAMPLING_RATE, ctypes.byref(ctypes.c_int(8000)))
    bits = SpeexBits()
    speex_bits_init(ctypes.byref(bits))
    frame_size = ctypes.c_int()
    speex_decoder_ctl(speex, SPEEX_GET_FRAME_SIZE, ctypes.byref(frame_size))

    output = ctypes.create_string_buffer(2000)
    offs = 0
    while offs < len(rec):
        nbytes = rec[offs]
        offs += 1
        if nbytes != 0x26:
            continue
        buf = ctypes.create_string_buffer(rec[offs:offs + nbytes])
        offs += nbytes
        speex_bits_read_from(ctypes.byref(bits), buf, ctypes.c_int(nbytes))
        # this loop looks strange, but its like in roger router and seems to work
        for i in range(2):
            rc = speex_decode_int(speex, ctypes.byref(bits), output)
            if rc == -1:
                break
            elif rc == -2:
                print("Decoding error: corrupted stream?");
                break
            if speex_bits_remaining(ctypes.byref(bits)) < 0:
                print("Decoding overflow: corrupted stream?");
                break
        wav.writeframes(output[0:2 * frame_size.value])

    wav.close()
    speex_bits_destroy(ctypes.byref(bits))


def process_meta(fn):
    with open(fn, "rb") as metaf:
        meta = metaf.read()

        for offs in range(0, len(meta), metalen):
            calldata = VoiceboxMeta.from_buffer_copy(meta[offs:offs + metalen])
            i = struct.unpack('<I', calldata.index)[0]
            if calldata.len != metalen:
                raise ValueError("wrong length")
            num = calldata.remote_number.decode('ascii')
            fn = calldata.file.decode('ascii')
            print("{0:03}  {1.day:02}.{1.month:02}.{1.year:02} {1.hour:02}:{1.minute:02}"
                  "  {1.duration:4}s  {2}".format(i, calldata, num))
            inp = os.path.join("rec", fn)
            ofn = "20{1.year:02}-{1.month:02}-{1.day:02}-" \
                    "{1.hour:02}:{1.minute:02}-{2}.wav".format(i, calldata, num)
            outp = os.path.join('wav', ofn)
            speex_convert(inp, outp)


if len(sys.argv) > 1:
    m = sys.argv[1]
else:
    m = 'meta0'
try:
    os.mkdir('wav')
except FileExistsError:
    pass
process_meta(m)
print("Done.")
